# Web Engineering
_Vienna University of Technology, Summer Term 2013_

## Syllabus
Nach erfolgreicher Absolvierung der Lehrveranstaltung sind die Studierenden mit Technologien und Entwicklungskonzepten zur Realisierung dynamischer Web-Anwendungen unter Berücksichtigung geltender Standards (u.a. Barrierefreiheit) vertraut.

 * M1: Einführung
 * M2: XHTML, HTML5, CSS, WAI (Tools, Validators, ...), JavaScript
 * M3: Servlet, Java Server Pages (JSP)
 * M4: Entwurfsmuster
 * M5: Java Server Faces (JSF, Gastvortrag: Martin Marinschek)
 * M6: Web Services
 * M7: Social Software + Advanced Topics of Web Application Development
 * M8: Ruby on Rails
 * M9: Open Linked Data
 * M10: App Entwicklung


## Exercises

### Done
 * UE1: XHTML, CSS, Javascript, WAI
 * UE2: Servlets, JSP
 * UE3: JSF
 * UE4: Rich Client Technologies and Web Services

### TODO
 * Nothing - All Done :-)

 
There is a seperate branch for each exercise.

## Contact
 * Manuel Geier
  	* [Website](http://www.manuelgeier.com)
    * via twitter: [My Profile](http://twitter.com/mangei)
 * Florian Mayer
 * Thomas Rieder
 	* [Blog](http://thomasrieder.github.com)
    * via twitter: [My Profile](https://twitter.com/#!/thomasrieder)
    * via email: thomasrieder _at_ aon _dot_ at
